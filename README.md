# For the API
- npm run start

- Runs the app in the development mode.

- Open http://localhost:8080 to view it in the browser.

    ## controller 

- Crud times

- Crud Users 

- Crud CellPosition:  function getInit used with query string with properties user id, start date, end date  (for display shifts in front and get data between two dates)


# Ror the REACT
- npm start

- Runs the app in the development mode.

- Open ``http://localhost:3000`` to view it in the browser.

- ui-material:  `npm install react-dnd `.

- Times of the week: ` npm install moment`.

- Manage data globaly for planning, times and users: ` npm install jotai`.

- Fetch data from api: ` npm install axios`.


    ## Components 
### AddShiftsComponents

- this component is for adding shifts to the template |homeComponet|.

### AddUserComponent 

- this component is for adding users .

### CellComponent

- it's a component for td tags means cellule of the table that is droppable, contain functions of drop to and delete the item that dropped to it.

### DragShifts 

- it's a draggable component .

### dropRowComponent

- contain tr tag and function to display the shifts that saved in the database in thier position acoording to user id, day and boxday.
- th tag for display user name.

### HomeComponet 

- contain all components and functions: get days of week also the next and previous and week,
functions to fetch users and shifts 

- endpoint of getinitial shifts (backend): used for get between two dates (filtring) and to display shifts also. 

