
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const recordsSchema = new Schema(
  {
    item: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Time'
    },
    user_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    boxDay : {
        type: Number,
    },
    day : {
      type: Date,
    },
  }
);

const Records = mongoose.model("Records", recordsSchema);

module.exports = Records;

