const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TimeSchema = new Schema(
  {
    startTime: { 
        type: String,
        lowercase: true,
        trim: true
    },
    endTime: { 
        type: String,
        lowercase: true,
        trim: true
    },
    bgColor: {
        type: String,
    }
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

const Time = mongoose.model("Time", TimeSchema);

module.exports = Time;

