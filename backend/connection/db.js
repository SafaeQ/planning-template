const mongoose = require('mongoose')
require('dotenv').config()

// create connection with ODM mongoose
async function db (){
    mongoose.connect(process.env.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true }, _err => {
        console.log(' 😊 WooW connected');
    })
}

module.exports = db;