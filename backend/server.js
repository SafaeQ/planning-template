const express = require('express')

const bodyParser = require('body-parser')

require("dotenv").config();

const cors = require('cors')

const db = require('./connection/db')

const path = require('path')

const PORT = process.env.PORT || 8080

// instance of express
const app = express()

//middlewares
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended: true}))

app.use(
    cors({
        origin: '*',
        optionsSuccessStatus: 200}
    ))


// routes
const userRouter = require('./routes/routes')
const timeRouter = require('./routes/times.routes')

app.use('/', userRouter, timeRouter)

// Serve vstatic files 
if (process.env.NODE_ENV === 'production') {
    // set static folder
    app.use(express.static('frontend/build'))

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'frontend', 'build', 'index.html'))
    })
}

// running the server
db().catch(
    (err) => {
        throw err
    }
).then(
    () => {
        app.listen(PORT, () => {
            console.log(` 🐱The server is runnig`);
        })
    }
)
