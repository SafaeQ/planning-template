const Joi = require("joi");

// validate the property of user 
const userValidation = (data) => {

    const schema = Joi.object({
        name: Joi.string().min(2).required()
    });

    return schema.validate(data);
};

module.exports = userValidation;
