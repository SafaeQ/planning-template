const userRouter = require('express').Router()

// import functions 
const {createUser, getAllUser}= require('../controller/user.controller')


// create routes for users

    userRouter.post('/' ,createUser)

    userRouter.get('/', getAllUser)
    


module.exports = userRouter
