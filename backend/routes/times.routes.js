const timeRouter = require('express').Router()

const {savePosition, getInit, removeItem} = require('../controller/cellPosition.controller')
// import functions 
const { createTimes, getAllTimes, deleteTimes, getSingleTime }= require('../controller/times.controller')


// create routes for users

    timeRouter.post('/time' ,createTimes)

    timeRouter.get('/time', getAllTimes)

    timeRouter.get('/time/:id', getSingleTime)

    timeRouter.delete('/time/:id', deleteTimes)


// save cell position routes

    timeRouter.post('/save', savePosition)

    timeRouter.get('/init', getInit)
    
    timeRouter.delete('/:id', removeItem)


module.exports = timeRouter
