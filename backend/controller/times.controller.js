const Time = require("../models/times.model");

// create times
const createTimes = async (req, res) => {
  try {
    const { startTime, endTime ,bgColor} = req.body;

    const time = await Time.create({ startTime, endTime, bgColor });

    if (!time) return res.status(404).send(" something wrong");

    const result = await time.save();

    res.status(200).send(result);
  } catch (error) {
    res.status(505).send(error);
  }
};

// get all the times
const getAllTimes = async (req, res) => {
  try {
    const times = await Time.find({});

    if (!times) return res.status(400).send("times not found");

    res.status(200).send(times);
  } catch (error) {
    res.status(505).send(error);
  }
};

// remove times by id
const deleteTimes = async (req, res) => {
  const id = req.params.id;

  let time;

  try {
    time = await Time.findByIdAndRemove(id)
    .catch((err) => {
      throw err;
    });

    res.status(200).json("removed successfully");
  } catch (error) {
    console.error(error);
  }
};

// get single time by id
const getSingleTime = async (req, res) => {
  const id = req.params.id;

  try {
    const time = await Time.findById(id)
    .catch((err) => {
      throw err;
    });

    res.status(200).send(time);
  } catch (error) {
    console.error(error);
  }
};

module.exports = { createTimes, getAllTimes, deleteTimes, getSingleTime };
