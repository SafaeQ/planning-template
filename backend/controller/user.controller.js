const User = require("../models/user.model");

const userValidation = require("../validation/user.validation");

// create user and save it in db
const createUser = async (req, res) => {
  const { name } = req.body;

  try {
    const { error } = userValidation(req.body);

    if (error) {
      return res.status(400).send(error.details[0].message);
    }

    const user = await User.create({ name });

    const result = await user.save();

    res.status(200).send(result);
  } catch (error) {
    res.status(400).send(error);
  }
};

// get the users from db
const getAllUser = (req, res) => {
  User.find()
    .then((data) => {
      let message = "";

      if (data === undefined || data.length == 0) message = "No user found!";
      else message = "users successfully retrieved";

      res.send(data);
    })
    .catch((err) => {
      res
        .status(500)
        .send({
          success: false,
          message: err.message || "Some error occurred while retrieving users.",
        });
    });
};

module.exports = { createUser, getAllUser };
