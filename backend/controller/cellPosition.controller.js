const Records = require("../models/cellPosition.model");

const savePosition = async (req, res) => {
  try {
    let cm = await Records.insertMany(req.body)
    res.status(200).send(cm);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getInit = async (req, res) => {
  try {
    const { start_date, end_date, user_id } = req.query;
    const query = {};
    user_id !== undefined && (query.user_id = user_id);
    if (start_date !== undefined && end_date !== undefined) {
      query.$and = [
        { day: { $gte: new Date(start_date).toISOString() } },
        { day: { $lte: new Date(end_date).toISOString() } },
      ];
    } else if (start_date !== undefined) {
      start_date !== undefined &&
        (query.day = { $gte: new Date(start_date).toISOString() });
    } else if (end_date !== undefined) {
      end_date !== undefined &&
        (query.day = { $lte: new Date(end_date).toISOString() });
    }
    let cm = await Records.find(query);
    res.status(200).send(cm);
  } catch (error) {
    res.status(400).send({ error: "something wrong" });
  }
};

const removeItem = async (req, res) => {
  try {
    let id = req.params.id;
    let item = await Records.findByIdAndRemove(id);
    res.status(200).send("Delete item successfully");
  } catch (error) {
    res.status(400).send(error);
  }
};

module.exports = { savePosition, getInit, removeItem };
