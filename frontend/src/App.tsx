import React from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { Routes, Route } from "react-router-dom";
import DragDrop from "./Components/Home";
import UserTable from "./UserComponents/UsersTable";

function App() {
  return (
    <>
      <DndProvider backend={HTML5Backend}>
        <Routes>
          <Route path="/" element={<DragDrop />} />
          <Route path="/users" element={<UserTable />} />
        </Routes>
      </DndProvider>
    </>
  );
}

export default App;
