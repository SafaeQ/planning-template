import DragShifts from "../Components/DragShifts";
import React, { useState } from "react";
import useTimesAtom from "../context/times.atom";
import usePlanningAtom from "../context/planning.atom";

function CelluleComponent({ boxDay, user_id, day }: any) {
  const [planning] = usePlanningAtom();
  const [p] = useState(planning);
  const [times] = useTimesAtom();
  let board: any = null;
  const setboard = (v: any) => (board = v);

  //display shifts in the table according to user_id and day
  let current: any;
  (() => {
    if (!Array.isArray(planning)) return;
    current = planning.find((p: any) => {
      return p.user_id == user_id && p.day == new Date(day).toISOString();
    });
    if (!current) return;
    const PlanningTime = times.find((t: any) => t._id == current.item);
    setboard(PlanningTime);
  })();

  return (
    <>
      {
        <td draggable="false" className="drop" key={boxDay}>
          <div>
            {board !== null && (
              <DragShifts
                key={boxDay}
                startTime={board?.startTime}
                endTime={board?.endTime}
                _id={board?.id}
                bgColor={board?.bgColor}
              />
            )}
          </div>
        </td>
      }
    </>
  );
}

export default CelluleComponent;
