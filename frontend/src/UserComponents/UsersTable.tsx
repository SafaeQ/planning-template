import React, { FC, useEffect, useState } from "react";
import "../Components/Home.css";
import UserRow from "./RowUserComponent";
import api from "../services/api";
import moment from "moment";
import { Layout } from "antd";
import { Content, Header } from "antd/lib/layout/layout";
import useTimesAtom from "../context/times.atom";
import useUsersAtom from "../context/users.atom";

const UserTable: FC = () => {
  // manege state globally
  const [users, setUsers] = useUsersAtom();
  const [times, setTimes] = useTimesAtom();

  const [weekCount] = useState(0);
  let [days] = useState(getWeekDays(weekCount));

  // get the days of the week using moment
  function getWeekDays(week: any) {
    let currentDate = moment();
    let weekStart = currentDate
      .clone()
      .add(week * 7, "day")
      .startOf("isoWeek");
    let days = [];
    for (let i = 0; i <= 6; i++) {
      days.push(moment(weekStart).add(i, "days").format("YYYY-MM-DD"));
    }
    return days;
  }

  // get times data from api using axios
  const fetchTimes = () => {
    api
      .get("/time")
      .then((res) => {
        setTimes(res.data);
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  };

  // get user data from api using axios
  const fetchUser = () => {
    api
      .get("/")
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  };

  useEffect(() => {
    fetchUser();
    fetchTimes();
  }, []);

  return (
    <>
      {times.length > 0 && (
        <Layout style={{ height: "100vh" }}>
          <Header>
            <img
              className="logo"
              src="https://m-job.ma/storage/app/uploads/public/628/ba7/c21/628ba7c216bd7621911859.jpg"
            />
          </Header>
          <Content>
            <div className="item">
              <h1 className="header">Planning of the week</h1>
              <div className="divs w-75 p-3 row justify-content-center">
                <h3 className="row justify-content-center"> Today </h3>
              </div>
              <div className="table w-75 p-3">
                <table className="table table-bordered shadow-sm p-3 mb-5 bg-white rounded ">
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      {days.map((day, i) => (
                        <th scope="col" key={Math.random()}>
                          {moment(day).locale("fr").format("dddd")}
                        </th>
                      ))}
                    </tr>

                    <tr>
                      <th scope="col">Mailers</th>
                      {days.map((day, i) => (
                        <th scope="col" key={i}>
                          {day}
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {users.map((user: any) => (
                      <UserRow
                        key={user._id}
                        name={user.name}
                        user_id={user._id}
                        days={days}
                      />
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </Content>
        </Layout>
      )}
    </>
  );
};

export default UserTable;
