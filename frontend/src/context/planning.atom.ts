import * as React from "react";
import {context, PlanningType} from "./usersContext"

export default function usePlanningAtom(): [ PlanningType[], React.Dispatch<React.SetStateAction<PlanningType[]>> ] {
    const state = React.useContext(context);
    const {planning,  setplanning} = state.planningState;
    return [planning, setplanning];
}