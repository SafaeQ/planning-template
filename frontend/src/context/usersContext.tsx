import * as React from "react";
import api from "../services/api";

export type TimeType = {
  _id: string;
  startTime: string;
  endTime: string;
  bgColor: string;
};

export type UserType = {
  _id: string;
  name: string;
};

export type PlanningType = {
  _id?: string;
  day: string;
  item: string;
  user_id: string;
  boxDay: number;
  isSaved: boolean;
};

type GlobalStateUser = {
  users: UserType[];
  setUsers: React.Dispatch<React.SetStateAction<UserType[]>>;
};

type GlobalStateTime = {
  times: TimeType[];
  setTimes: React.Dispatch<React.SetStateAction<TimeType[]>>;
};

type GlobalStatePlanning = {
  planning: PlanningType[];
  setplanning: React.Dispatch<React.SetStateAction<PlanningType[]>>;
};

type GlobalStateState = {
  usersState: GlobalStateUser;
  timesState: GlobalStateTime;
  planningState: GlobalStatePlanning;
};

export const context = React.createContext<GlobalStateState>(
  {} as GlobalStateState
);

const Provider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  let [users, setUsers] = React.useState<UserType[]>([]);
  let [times, setTimes] = React.useState<TimeType[]>([]);
  let [planning, setplanning] = React.useState<PlanningType[]>([]);

  // get user data from api using axios
  const fetchUser = () => {
    api
      .get<UserType[]>("/")
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  };

  // get times data from api using axios
  const getTimes = () => {
    api
      .get<TimeType[]>("/time")
      .then((res) => {
        setTimes(res.data);
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  };

  React.useEffect(() => {
    fetchUser();
    getTimes();
  }, []);

  return (
    <context.Provider
      value={{
        usersState: { users, setUsers },
        timesState: { times, setTimes },
        planningState: { planning, setplanning },
      }}
    >
      {children}
    </context.Provider>
  );
};

export default Provider;
