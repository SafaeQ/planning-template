import * as React from "react";
import { context, TimeType } from "./usersContext";

export default function useTimesAtom(): [
  TimeType[],
  React.Dispatch<React.SetStateAction<TimeType[]>>
] {
  const state = React.useContext(context);
  const { times, setTimes } = state.timesState;
  return [times, setTimes];
}
