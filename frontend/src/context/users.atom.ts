import * as React from "react";
import { context, UserType } from "./usersContext";

export default function useUsersAtom(): [
  UserType[],
  React.Dispatch<React.SetStateAction<UserType[]>>
] {
  const state = React.useContext(context);
  const { users, setUsers } = state.usersState;
  return [users, setUsers];
}
