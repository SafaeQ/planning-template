import React, { useState } from "react";
import styles from "./Modal.module.css";
import { RiCloseLine } from "react-icons/ri";
import api from "../services/api";
import { message } from "antd";
import useUsersAtom from "../context/users.atom";

interface Props {
  setIsOpen: (open: boolean) => void;
}

const AddUserModal = ({ setIsOpen }: Props) => {
  const [name, setName] = useState("");
  
  // manage users globaly using jotai
  const [users, setUsers] = useUsersAtom();

  // handle change of input
  function handleChangeName(event: any) {
    setName(event.target.value);
  }

  // function for submit form and send data with axios
  const submitHandler = (e: any) => {
    e.preventDefault();

    let data = {
      name: name,
    };

    api
      .post("/", data)
      .then((res) => {
        setUsers([...users, res.data]);
        setIsOpen(false);
        message.success("The User is added");
      })
      .catch((err) => {
        message.success("Somthing went wrong");
      });
  };

  return (
    <>
      <div className={styles.darkBG} onClick={() => setIsOpen(false)} />
      <div className={styles.centered}>
        <div className={styles.modal}>
          <div className={styles.modalHeader}>
            <h5 className={styles.heading}>Add New Mailer</h5>
          </div>
          <button className={styles.closeBtn} onClick={() => setIsOpen(false)}>
            <RiCloseLine style={{ marginBottom: "-3px" }} />
          </button>
          <div className={styles.modalContent}>
            <label style={{ fontWeight: "bold" }}> user's name </label>
            <input
              className="form-control"
              name="name"
              type="text"
              onChange={handleChangeName}
            />
          </div>
          <div className={styles.modalActions}>
            <div className={styles.actionsContainer}>
              <button className={styles.okBtn} onClick={submitHandler}>
                OK
              </button>
              <button
                className={styles.cancelBtn}
                onClick={() => setIsOpen(false)}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddUserModal;
