import DragShifts from "./DragShifts";
import React, { useEffect, useState } from "react";
import { DropTargetMonitor, useDrop } from "react-dnd";
import api from "../services/api";
import { message, Popconfirm } from "antd";
import useTimesAtom from "../context/times.atom";
import usePlanningAtom from "../context/planning.atom";
import { TimeType } from "../context/usersContext";

type CellProps = {
  boxDay: number;
  user_id: string;
  day: string;
};

function CellComponent({ boxDay, user_id, day }: CellProps) {
  const [planning, setplanning] = usePlanningAtom();
  const [p] = useState(planning);
  const [times] = useTimesAtom();
  const [hasDropped, setHasDropped] = useState(false);
  const [board, setboard] = useState<TimeType | null>(null);

  const [{ isOver, canDrop }, drop] = useDrop(() => ({
    accept: "p",
    drop: addItem,
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  }));

  //display shifts in the table according to user_id and day
  let current: any;
  useEffect(() => {
    console.log({planning})
    if (!Array.isArray(planning)) return;
    current = planning.find((p: any) => {
      return p.user_id == user_id && p.day == new Date(day).toISOString();
    });
    console.log({current,user_id})
    if (!current) return;
    const PlanningTime: any = times.find((t: any) => t._id == current.item);
    console.log({PlanningTime})
    setboard(PlanningTime);
  }, [planning]);

  // add item boxDay and user_is with time_id in mongodb
  function addItem(item: TimeType, monitor: DropTargetMonitor<any, void>) {
    // setting planning and push to it new elements
    setplanning((planning) => {
      planning.push({
        item: item._id,
        boxDay,
        user_id,
        day,
        isSaved: false,
      });
      return planning;
    });

    const didDrop = monitor.didDrop();
    if (didDrop) {
      return;
    }
    setHasDropped(true);
    const board = times.find((t) => t._id == item._id);
    if (board) setboard(board);
  }

  // delete shifts request
  function deleteItem() {
    // current => get one element from the list of planning
    current = planning.find((p: any) => {
      return p.user_id == user_id && p.day == new Date(day).toISOString();
    });
    api
      .delete(`/${current._id}`)
      .then((res) => {
        setplanning((prev) => prev.filter((e: any) => e._id !== current._id));
        message.success("The Shift is deleted");
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  }

  const cancel = (e: any) => {
    message.error("Is Canceled");
  };

  return (
    <>
      {
        <div style={{minHeight:"3rem"}} draggable="true" className="drop" ref={drop} key={boxDay}>
          <Popconfirm
            title="Are you sure to delete this Shift?"
            onConfirm={deleteItem}
            onCancel={cancel}
            okText="Yes"
            cancelText="No"
          >
            <div>
              {board !== null && (
                <>
                  <DragShifts
                    key={boxDay}
                    startTime={board?.startTime}
                    endTime={board?.endTime}
                    _id={board?._id}
                    bgColor={board?.bgColor}
                  />
                </>
              )}
            </div>
          </Popconfirm>
        </div>
      }
    </>
  );
}

export default CellComponent;
