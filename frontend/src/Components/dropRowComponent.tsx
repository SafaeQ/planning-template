import { useEffect } from "react";
import usePlanningAtom from "../context/planning.atom";
import api from "../services/api";
import CellComponent from "./CellComponent";

const DropRow = ({ name, user_id, days }: any) => {
  const [planning, setplanning] = usePlanningAtom();
  const [start_date] = days;
  const end_date = days[days.length - 1];

  // get shifts data from api using axios
  useEffect(() => {
    const path = `/init?user_id=${user_id}&start_date=${new Date(
      start_date
    ).toISOString()}&end_date=${new Date(end_date).toISOString()}`;
    api
      .get(path)
      .then((res) => {
        setplanning((prev) => {
          return [...prev, ...res.data];
        });
      })
      .catch((err) => {
        return Promise.reject(err);
      });
  }, [days]);

  return (
    <tr>
      <th scope="row">{name}</th>
      {days.map((_: any, boxDay: number) => (
        <CellComponent
          boxDay={boxDay}
          key={Math.random()}
          user_id={user_id}
          day={days[boxDay]}
        />
      ))}
    </tr>
  );
};

export default DropRow;
