import { FC } from "react";
import { DragSourceMonitor, useDrag } from "react-dnd";

type DragProps = {
  _id: string;
  startTime?: string;
  endTime?: string;
  bgColor?: string;
};

const DragShifts: FC<DragProps> = ({ _id, startTime, endTime, bgColor }) => {
  // drag shifts
  const [_, drag] = useDrag<{ _id: string }>(() => ({
    type: "p",
    item: { _id: _id },
    collect: (monitor: DragSourceMonitor<unknown>) => ({
      isDragging: monitor.isDragging(),
    }),
  }));

  return (
    <p
      ref={drag}
      className="draggable"
      draggable="true"
      style={{ backgroundColor: bgColor }}
    >
      {startTime} {endTime}
    </p>
  );
};

export default DragShifts;
