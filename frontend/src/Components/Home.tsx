import React, { FC, useState } from "react";
import DragShifts from "./DragShifts";
import "./Home.css";
import DropRow from "./dropRowComponent";
import moment from "moment";
import {
  Button,
  Layout,
  DatePicker,
  Popconfirm,
  message,
  Table,
  Modal,
  Form,
  Input,
} from "antd";
import { Content } from "antd/lib/layout/layout";
import { FcNext, FcPrevious, FcPlus } from "react-icons/fc";
import { TiDelete } from "react-icons/ti";
import useUsersAtom from "../context/users.atom";
import useTimesAtom from "../context/times.atom";
import usePlanningAtom from "../context/planning.atom";
import { DndProvider, DropTargetMonitor, useDrop } from 'react-dnd';
import { HTML5Backend } from "react-dnd-html5-backend";
import type { ColumnProps } from "antd/lib/table";
import { Switch } from "antd";
import dayjs from "dayjs";
import { TimeType, UserType } from "../context/usersContext";
import { useEffect } from 'react';
import CellComponent from "./CellComponent";
import CelluleComponent from "../UserComponents/CelluleComponent";
import api from "../services/api";


const DragDrop: FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [toggle, setToggle] = React.useState(false);
  const [weekCount, setWeekCount] = useState(0);
  let [days, setDays] = useState(getWeekDays(weekCount));
  const [selectedDate]: any = useState(null);
  const [startTime, setStartTime] = React.useState("");
  const [endTime, setEndTime] = React.useState("");
  const [bgColor, setBgColor] = React.useState("");
  // manage state globally
  const [users, setUsers] = useUsersAtom();
  const [times, setTimes] = useTimesAtom();
  const [planning, setplanning] = usePlanningAtom();

  //*************** Add times *****************//

  // handle change of input for BreakTime
  function handleChangeBreak(event: any) {
    setStartTime(event.target.value);
  }

  // handle change of input for startTime
  function handleChangeStartTime(event: any) {
    setStartTime(event.target.value);
  }

  // handle change of input for endTime
  function handleChangeEndTime(event: any) {
    setEndTime(event.target.value);
  }

  // handle change of input for bgColor
  function handleChangeBgColor(event: any) {
    setBgColor(event.target.value);
  }

  const toggler = () => {
    toggle ? setToggle(false) : setToggle(true);
  };

  const handleCancel = () => {
    setIsOpen(false);
  };

  // add shifts
  const hundleSubmit = (e: any) => {
    e.preventDefault();

    let dataTime = {
      startTime: startTime,
      endTime: endTime,
      bgColor: bgColor,
    };
    api
      .post("/time", dataTime)
      .then((res: any) => {
        setTimes([...times, res.data]);
        setIsOpen(false);
        message.success("The Shift is added");
      })
      .catch((err: any) => {
        message.error("Somthing went wrong");
      });
  };

  //***************  *****************//

  // save btn
  const savedShifts = () => {
    let unsavedShifts = planning.filter((p) => p.isSaved === false);
    api
      .post("/save", unsavedShifts)
      .then((res: any) => {
        if (res.status === 200) {
          message.success("The Shift is successfully saved");
        }
      })
      .catch((err: any) => {
        message.error("Somthing went wrong");
        return Promise.reject(err);
      });
  };

  // get the days of the week using moment
  function getWeekDays(week: any) {
    let currentDate = moment();
    let weekStart = currentDate
      .clone()
      .add(week * 7, "day")
      .startOf("isoWeek");
    let days = [];
    for (let i = 0; i <= 6; i++) {
      days.push(moment(weekStart).add(i, "days").format("YYYY-MM-DD"));
    }
    return days;
  }

  // setting the next week in table
  const nextPage = () => {
    if (weekCount < 1) {
      setWeekCount((next) => {
        setDays(() => getWeekDays(next + 1));
        return next + 1;
      });
    }
  };

  // get the currentWeek
  const currentWeek = () => {
    setDays(() => getWeekDays(days));
    return days;
  };

  // setting the previous week in table
  const prevPage = () => {
    if (weekCount > -20) {
      setWeekCount((prev) => {
        setDays(() => getWeekDays(prev - 1));
        return prev - 1;
      });
    }
  };

  /** @datePicker attribute */
  const { RangePicker } = DatePicker;

  const [start_date] = days;
  const end_date = days[days.length - 1];

  // get get the data and days between to dates
  const filtring: (dates: any) => void = function ([start, end]) {
    const dys = [start.format("YYYY-MM-DD")];
    while (start.diff(end, "days") != 0) {
      start.add(1, "days");
      dys.push(start.format("YYYY-MM-DD"));
    }
    setDays(() => dys);
  };

  // deleteShifts after dropped
  const deleteShifts = (time: any) => {
    api
      .delete(`/shifts/${time.id}`)
      .then((res: any) => {
        setTimes(times.filter((t: any) => t._id !== time.id));
        message.success("The Shift is deleted");
      })
      .catch((err: any) => {
        return Promise.reject(err);
      });
  };
  const cancel = () => {
    message.error("Canceled");
  };

    // get shifts data from api using axios
    useEffect(() => {
      const path = `/init?start_date=${new Date(
        start_date
      ).toISOString()}&end_date=${new Date(end_date).toISOString()}`;
      api
        .get(path)
        .then((res) => {
          console.log("planning",res.data)
          setplanning((prev) => {
            return [...prev, ...res.data];
          });
        })
        .catch((err) => {
          return Promise.reject(err);
        });
    }, [days]);

// ****************************************************************
  const childs: any = days.map((day, i) => ({
    title: dayjs(day).format("dddd"),
    children: [
      {
        title: day,
        render:(user:UserType)=> { 
        console.log("rerendered")
        return <CellComponent key={Math.random()} boxDay={i} user_id={user._id} day={day}/>
      },    
      },
    ],
    dataIndex: "date",
    width: 156,
  }));

  const datacolumn: any = [
    {
      title: "mailers",
      dataIndex: "",
      render: (user: any) => <a>{user.name} </a>,
    },
    {
      title: "Days of week",
      children: childs,
    },
  ];

  return (
      <Layout style={{ height: "100vh" }}>
        <Content>
          <div className="item">
            <h1 className="header">Planning of the week</h1>
            <div className="divs w-75 p-3">
              <div id="external-events">
                <p style={{ paddingRight: "3rem", alignItems: "center" }}>
                  <strong> Holidays </strong>
                </p>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    gap: "0.5rem",
                  }}
                >
                  <div key={Math.random()}>
                    <DragShifts
                      _id={Math.random().toString(8)}
                      bgColor={"#60dfff"}
                      startTime={"EidAdhah"}
                    />
                  </div>
                  <div key={Math.random()}>
                    <DragShifts
                      _id={Math.random().toString(8)}
                      bgColor={"#10dfaf"}
                      startTime={"Achoraa"}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div id="external-events">
              <button className="closeBtn" onClick={() => setIsOpen(true)}>
                <FcPlus style={{ marginBottom: "-3px" }} />
              </button>
              <Modal
                title="Add User"
                visible={isOpen}
                onCancel={handleCancel}
                onOk={hundleSubmit}
              >
                <Form>
                  <div>
                    <span>switch me </span>
                    <Switch
                      onClick={toggler}
                      checkedChildren="Break"
                      unCheckedChildren="Times"
                    />
                  </div>
                  {toggle ? (
                    <Form.Item
                      label="Break time"
                      name="name"
                      rules={[
                        {
                          required: true,
                          message: "Please input your Break time!",
                        },
                      ]}
                      style={{ padding: 20 }}
                    >
                      <Input onChange={handleChangeBreak} size="large" />
                    </Form.Item>
                  ) : (
                    <>
                      <Form.Item
                        label="Start Time"
                        name="Start Time"
                        rules={[
                          {
                            required: true,
                            message: "Please input your Start Time!",
                          },
                        ]}
                        style={{ padding: 20 }}
                      >
                        <Input
                          onChange={handleChangeStartTime}
                          size="large"
                          type="time"
                        />
                      </Form.Item>
                      <Form.Item
                        label="End Time"
                        name="End Time"
                        rules={[
                          {
                            required: true,
                            message: "Please input your End Time!",
                          },
                        ]}
                        style={{ padding: 20 }}
                      >
                        <Input
                          onChange={handleChangeEndTime}
                          size="large"
                          type="time"
                        />
                      </Form.Item>
                    </>
                  )}
                  <Form.Item
                    label=" Pick a color"
                    name="color"
                    rules={[
                      {
                        required: true,
                        message: "Please input your Color!",
                      },
                    ]}
                    style={{ padding: 20 }}
                  >
                    <Input onChange={handleChangeBgColor} type="color" />
                  </Form.Item>
                </Form>
              </Modal>
              <p style={{ paddingRight: "3rem", alignItems: "center" }}>
                <strong> Timing </strong>
              </p>
              <div
                className="fc-event d-flex justify-content-between gap-3"
                style={{ display: "flex", flexDirection: "row", gap: "0.5rem" }}
              >
                {times.map((time: any) => {
                  return (
                    <div key={Math.random()}>
                      <Popconfirm
                        title="Are you sure to delete this Shift?"
                        onConfirm={() => deleteShifts(time)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                      >
                        <TiDelete />
                      </Popconfirm>
                      <DragShifts
                        _id={time._id}
                        bgColor={time.bgColor}
                        startTime={time.startTime}
                        endTime={time.endTime}
                        key={time._id}
                      />
                    </div>
                  );
                })}
              </div>
            </div>

            <div className="divBtn">
              <Button className="prevBtn" onClick={prevPage}>
                <span>
                  <FcPrevious />
                </span>
                Previous
              </Button>
              <div className="dateRanger">
                <RangePicker onChange={filtring} showNow />
              </div>
              <Button className="nextBtn" onClick={currentWeek}>
                Current
              </Button>
              <Button className="nextBtn" onClick={nextPage}>
                <span>
                  <FcNext />
                </span>
                Next
              </Button>
            </div>
            <div>
              <Button className="publishBtn" onClick={savedShifts}>
                Save
              </Button>
            </div>
            <Table
              columns={datacolumn}
              rowKey="id"
              dataSource={users}
              bordered
              className="antdt"
            />
          </div>
        </Content>
      </Layout>
  );
};

export default DragDrop;
