import React from "react";
import styles from "./TimeModal.module.css";
import { RiCloseLine } from "react-icons/ri";
import api from "../services/api";
import { message, Switch } from "antd";
import useTimesAtom from "../context/times.atom";

interface Props {
  setIsOpenTime: (open: boolean) => void;
}


const AddTimes = ({ setIsOpenTime }: Props) => {
  const [startTime, setStartTime] = React.useState("");
  const [endTime, setEndTime] = React.useState("");
  const [bgColor, setBgColor] = React.useState("");
  const [toggle, setToggle] = React.useState(false);
    
  // manage data globally
  const [times, setTimes] = useTimesAtom();

  // handle change of input for Break
  function handleChangeBreak(event: any) {
    setStartTime(event.target.value);
  }

  // handle change of input for startTime
  function handleChangeStartTime(event: any) {
    setStartTime(event.target.value);
  }

  // handle change of input for endTime
  function handleChangeEndTime(event: any) {
    setEndTime(event.target.value);
  }

  // handle change of input for bgColor
  function handleChangeBgColor(event: any) {
    setBgColor(event.target.value);
  }

  // hundle Submit form send post request to api using axios
  const hundleSubmit = (e: any) => {
    e.preventDefault();
    const timePattern = /(?:[01]\d|2[0-3]):(?:[0-5]\d)/;
    // const breakTimePattern = /([A-z])\w+/

    // if(!timePattern.test(startTime) && !timePattern.test(endTime)){
    //   message.error("Please enter a valid time pattern");
    //   return
    // }

    let dataTime = {
      startTime: startTime,
      endTime: endTime,
      bgColor: bgColor,
    };
    api
      .post("/time", dataTime)
      .then((res) => {
        setTimes([...times, res.data]);
        setIsOpenTime(false);
        message.success("The Shift is added");
      })
      .catch((err) => {
        message.error("Somthing went wrong");
      });
  };

  const toggler = () => {
    toggle ? setToggle(false) : setToggle(true);
  };

  return (
    <>
      <div className={styles.darkBG} onClick={() => setIsOpenTime(false)} />
      <div className={styles.centered}>
        <div className={styles.modal}>
          <div className={styles.modalHeader}>
            <h5 className={styles.heading}>Add Timer</h5>
          </div>
          <button
            className={styles.closeBtn}
            onClick={() => setIsOpenTime(false)}
          >
            <RiCloseLine style={{ marginBottom: "-3px" }} />
          </button>
          <div className={styles.switch}>
            <span>switch me </span>
            <Switch onClick={toggler} checkedChildren="Break" unCheckedChildren="Times" />
          </div>
          {toggle ? (
            <div className={styles.modalContent}>
              <label style={{ fontWeight: "bold" }}> Break </label>
              <input
                className="form-control"
                name="breakTime"
                placeholder="..."
                type= 'text'
                onChange={handleChangeBreak}
              />
            </div>
          ) : (
            <>
              <div className={styles.modalContent}>
                <label style={{ fontWeight: "bold" }}> Start Time</label>
                <input
                  className="form-control"
                  name="startTime"
                  type="time"
                  onChange={handleChangeStartTime}
                />
              </div>
              <div className={styles.modalContent}>
                <label style={{ fontWeight: "bold" }}> End Time </label>
                <input
                  className="form-control"
                  name="endTime"
                  type="time"
                  onChange={handleChangeEndTime}
                />
              </div>
            </>
          )}
          <div className={styles.modalContent}>
            <label style={{ fontWeight: "bold" }}> Pick a color </label>
            <input
              className="form-control"
              type="color"
              onChange={handleChangeBgColor}
            />
          </div>
          <div className={styles.modalActions}>
            <div className={styles.actionsContainer}>
              <button className={styles.okBtn} onClick={hundleSubmit}>
                OK
              </button>
              <button
                className={styles.cancelBtn}
                onClick={() => setIsOpenTime(false)}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddTimes;
